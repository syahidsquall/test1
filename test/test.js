let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
let app = "http://localhost:8080/"
chai.use(chaiHttp);
describe('/GET /', () => {
      it('should return 200 status', function() {
      chai.request(app)
      .get('/')
      .end(function(err, res) {
        res.to.have.status(200);
        });
    });

    it('should return simple json', function() {
    chai.request(app)
    .get('/')
    .end(function(err, res) {
      res.should.be.json;
      });
  });
});
